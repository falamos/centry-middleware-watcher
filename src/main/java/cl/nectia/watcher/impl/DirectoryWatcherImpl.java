package cl.nectia.watcher.impl;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import cl.nectia.watcher.interfaz.DirectoryWatcher;

@Service
public class DirectoryWatcherImpl implements DirectoryWatcher {
	// propiedades para watcher
	private ResourceBundle watcherParam = ResourceBundle.getBundle("watcher");
	
	/**
	 * metodo encargado de monitorear una carpeta y llamar al servicio de ftp del proyecto 
	 * centry middleware custom al encontrar algun archivo entrante en la carpeta
	 */
	public void startWatcher() {

		Path path = Paths.get(watcherParam.getString("watcher_path"));
		try {
			WatchService watcher = path.getFileSystem().newWatchService();
			path.register(watcher, StandardWatchEventKinds.ENTRY_MODIFY);
			System.out.println("Monitoring directory for changes...");
			WatchKey watchKey = watcher.take();
			List<WatchEvent<?>> events = watchKey.pollEvents();
			for (WatchEvent event : events) {
				if (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY) {
					System.out.println("Modify: " + event.context().toString());
					final String uri = watcherParam.getString("watcher_uri_ftp_service");
					CallFtpProcess stot = new CallFtpProcess(uri+"?archivo="+event.context().toString());
					stot.start();
				
				}
			}
			startWatcher();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}