package cl.nectia.watcher;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import cl.nectia.watcher.interfaz.DirectoryWatcher;

@SpringBootApplication
public class DirectoryWatcherApplication {
	@Autowired
	public DirectoryWatcher directoryWatcher;
	public static void main(String[] args) {
		SpringApplication.run(DirectoryWatcherApplication.class, args);
	}
	 @PostConstruct
	    private void init() {
		 directoryWatcher.startWatcher();
	    }
}
